FROM php:apache-buster

RUN apt-get update && apt-get install -y \
    gcc make autoconf libc-dev pkg-config \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    wget \
    zip \
    unzip \
    libmcrypt-dev \
    libsodium-dev && \
    # mcrypt
    pecl install mcrypt-1.0.3 && \
    # We install and enable php-gd
    docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ && \
    docker-php-ext-install -j$(nproc) gd sodium && \
    docker-php-ext-enable mcrypt && \
    # We enable Apache's mod_rewrite
    a2enmod rewrite

# Copy app content
COPY ./privatebin /var/www/html

# Copy start script
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

VOLUME /var/www/html/data

ENTRYPOINT ["/entrypoint.sh"]
